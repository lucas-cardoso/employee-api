from django.contrib import admin
from django.urls import path, include
from django.conf.urls import url
from rest_framework import routers
from employee.views import EmployeeViewSet, DepartmentViewSet

router = routers.DefaultRouter()
router.register('employee', EmployeeViewSet)
router.register('department', DepartmentViewSet)

urlpatterns = [
    path('admin/', admin.site.urls),
    url('', include(router.urls)),
    url('api-auth/', include('rest_framework.urls'))
]

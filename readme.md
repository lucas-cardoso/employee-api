# Employee API
API para cadastro de Empregados e Departamentos
## Instalação (LINUX)
```
git clone https://lucas-cardoso@bitbucket.org/lucas-cardoso/employee-api.git
cd employee-api
```
```
virtualenv venv -p python3
source venv/bin/activate
```
```
pip install -r requirements.txt
```
```
python manage.py migrate
python manage.py createsuperuser
python manage.py makemigrations employeee
python manage.py migrate employeee
```
```
python manage.py runserver
```
## Sobre
Nesta API você pode cadastrar os departamentos e depois relacionar os empregados, e você tem o ambiente administrativo no endereço http://localhost:8000/admin/, onde você consegue gerenciar também.

## Documentação API
https://documenter.getpostman.com/view/4488948/S1a4X6eW?version=latest


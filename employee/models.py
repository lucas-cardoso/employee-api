from django.db import models

# Create your models here.
class Employee(models.Model):
    name = models.CharField('Nome', max_length=200)
    email = models.CharField('Email', max_length=200)
    department = models.ForeignKey('Department', on_delete=models.SET_NULL, related_name='employee', null=True, blank=True)

    def __str__(self):
        return self.name



class Department(models.Model):
    name = models.CharField('Nome', max_length=200)

    def __str__(self):
        return self.name
    
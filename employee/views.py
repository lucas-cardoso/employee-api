from django.shortcuts import render
from rest_framework import viewsets
from .api.serializers import EmployeeSerializer, DepartmentSerializer
from .models import Employee, Department


class EmployeeViewSet(viewsets.ModelViewSet):
    queryset = Employee.objects.all()
    http_method_names = ['get', 'post', 'delete']
    serializer_class = EmployeeSerializer

class DepartmentViewSet(viewsets.ModelViewSet):
    queryset = Department.objects.all()
    http_method_names = ['get', 'post', 'delete']
    serializer_class = DepartmentSerializer